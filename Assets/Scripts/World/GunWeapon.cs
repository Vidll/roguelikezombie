using UnityEngine;

public class GunWeapon : MapWeapon
{
	[SerializeField] private Gun _gunWeapon;

	protected override void SetWeapon(Player player)
	{
		base.SetWeapon(player);
		player.SetGun(_gunWeapon);
	}
}
