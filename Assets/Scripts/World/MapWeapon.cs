using UnityEngine;

public abstract class MapWeapon : MonoBehaviour
{
    [SerializeField] private Transform _weapon;
	[SerializeField] private WeaponSpawner _spawner;
    [SerializeField] private float _rotateWeaponSpeed;
	[SerializeField] private float _lifeTime = 60;

	private float _currentLifeTime = 0;
    private Transform _myTransform;

	public void Init(WeaponSpawner spawner)
	{
		_spawner = spawner;
		GlobalEvents.MapCreated += DeleteThisWeapon;
	}
	private void Update()
	{
		_weapon.Rotate(Vector3.up * _rotateWeaponSpeed * Time.deltaTime);
		_currentLifeTime += Time.deltaTime;
		if (_currentLifeTime >= _lifeTime)
		{
			_currentLifeTime = 0;
			DeleteThisWeapon();
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.TryGetComponent(out Player player))
		{
			SetWeapon(player);
			DeleteThisWeapon();
		}
	}

	protected virtual void SetWeapon(Player player)
	{

	}


	private void DeleteThisWeapon()
	{
		GlobalEvents.MapCreated -= DeleteThisWeapon;
		_spawner.DeleteWeapon(gameObject);
	}
}
