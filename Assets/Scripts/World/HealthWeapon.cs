using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthWeapon : MapWeapon
{
	[SerializeField] private float _increseHealth;
	[SerializeField] private float _increaseCoefficient;

	protected override void SetWeapon(Player player)
	{
		base.SetWeapon(player);
		var incrHealth = _increseHealth + (_increaseCoefficient * GlobalParameters.Level);
		//print("Set health: " + incrHealth);
		player.IncreaseHealth(player.Health + incrHealth);
	}
}
