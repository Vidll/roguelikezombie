using UnityEngine;
using System.Collections.Generic;

public class WeaponSpawner : MyComponentClass
{
	[SerializeField] private MapManager _mapManager;
	[SerializeField] private GunWeapon[] _weaponGuns;
	[SerializeField] private HealthWeapon[] _weaponHealth;
	[SerializeField] private BoxCollider _spawnCollider;
	[SerializeField] private Transform _startSpaw;

	[SerializeField] private List<GameObject> _createdWeapon;

	public override void StartGame()
	{
		base.StartGame();
		GlobalEvents.PassedWave += CreateWeapon;
		GlobalEvents.MapCreated += SetValuesAndCreateGun;
		_mapManager = GetComponentInParent<MapManager>();
	}
	public override void PlayGame()
	{
		base.PlayGame();
	}

	public override void LoseGame()
	{
		base.LoseGame();
		GlobalEvents.PassedWave -= CreateWeapon;
	}

	public override void ReloadGame()
	{
		base.ReloadGame();
	}

	private void SetValuesAndCreateGun()
	{
		_spawnCollider = _mapManager.CurrentMap.WeaponsSpawnPoint.GetComponent<BoxCollider>();
		_startSpaw = _mapManager.CurrentMap.StartSpawnPoint;
		CreateGun(true);
	}

	private void CreateWeapon()
	{
		var value = GetRandomValue();
		if (value > 70)
			CreateGun(false);
		else
			CreateHealth();
	}

	private GameObject Create(GameObject weapon, Vector3 spawPoint)
	{
		var newWeapon = Instantiate(weapon, spawPoint, Quaternion.identity);
		newWeapon.GetComponent<MapWeapon>().Init(this);
		newWeapon.transform.parent = transform;
		return newWeapon;
	}

	private void CreateWeapon(GameObject weapon)
	{
		var spawnPoint = new Vector3
			((_spawnCollider.transform.position.x + Random.Range(-_spawnCollider.size.x, _spawnCollider.size.x)),
			_spawnCollider.transform.position.y,
			(_spawnCollider.transform.position.z + Random.Range(-_spawnCollider.size.z, _spawnCollider.size.z)));
		var obj = Create(weapon, spawnPoint);
		_createdWeapon.Add(obj);
	}

	private void CreateFirstObject(GameObject needObject)
	{
		var spawPoint = _startSpaw.position;
		Create(needObject, spawPoint);
	}
	
	public void DeleteWeapon(GameObject needObject)
	{
		_createdWeapon.Remove(needObject);
		Destroy(needObject.gameObject);
	}

	public void WeaponTaked(GameObject takedObject)
	{
		_createdWeapon.Remove(takedObject);
	}

	[ContextMenu("Delete all weapons")]
	private void DeleteAllWeapons()
	{
		foreach(GameObject weapon in _createdWeapon)
		{
			Destroy(weapon);
		}
		_createdWeapon.Clear();
	}

	[ContextMenu("Create gun")]
	private void CreateGun(bool startPos)
	{
		if (startPos)
		{
			CreateFirstObject(_weaponGuns[Random.Range(0, _weaponGuns.Length)].gameObject);
			return;
		}
		CreateWeapon(_weaponGuns[Random.Range(0, _weaponGuns.Length)].gameObject);
	}

	[ContextMenu("Create health")]
	private void CreateHealth() => CreateWeapon(_weaponHealth[Random.Range(0, _weaponHealth.Length)].gameObject);
	private int GetRandomValue() => Random.Range(0, 100);
	
}
