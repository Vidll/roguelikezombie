using UnityEngine;

public class MapManager : MyComponentClass
{
    [SerializeField] private Map[] _maps;
	[SerializeField] private Map _currentMap;

	[SerializeField] private int _changeMapValue = 5;
	[SerializeField] private int _currenChangeMapValue;

	public Map CurrentMap { get => _currentMap; }

	public override void StartGame()
	{
		base.StartGame();
		_currenChangeMapValue = _changeMapValue;
	}

	public override void PlayGame()
	{
		base.PlayGame();
		CreateMap(0);
		GlobalInterfaceEvents.IncreacePlayer += CheckChangeMap;
	}

	public override void ReloadGame()
	{
		base.ReloadGame();
		_currenChangeMapValue = _changeMapValue;
		Destroy(_currentMap.gameObject);
	}

	private void CreateMap(int index)
	{
		_currentMap = Instantiate(_maps[index]);
		_currentMap.transform.parent = transform;
		_currentMap.InitMap();
	}

	private void ChangeMap()
	{
		Destroy(_currentMap.gameObject);
		CreateMap(Random.Range(0, _maps.Length));
	}

	private void CheckChangeMap()
	{
		if (_currenChangeMapValue <= 0)
		{
			GlobalEvents.StartChangeMap?.Invoke();
			ChangeMap();
			_currenChangeMapValue = _changeMapValue;
		}
		_currenChangeMapValue--;
	}
}
