using UnityEngine;

public class Map : MonoBehaviour
{
	public Transform[] EnemiesSpawnPoints;
	public Transform PlayerSpawnPoint;
	public GameObject WeaponsSpawnPoint;
	public Transform StartSpawnPoint;

	public void InitMap()
	{
		GlobalEvents.MapCreated?.Invoke();
	}
}
