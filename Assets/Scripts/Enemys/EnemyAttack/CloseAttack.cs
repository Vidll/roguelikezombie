using UnityEngine;

public class CloseAttack : MonoBehaviour
{
	[SerializeField] private float _damage;
	private Enemy _enemy;

	public float Damage { set { _damage = value; } get => _damage; }

	private void OnEnable()
	{
		_enemy = transform.parent.GetComponent<Enemy>();
	}

	private void OnTriggerStay(Collider other)
	{
		if(other.TryGetComponent(out Player player))
		{
			if (_enemy.CheckTimeAttack())
				player.GetDamage(_damage);
		}
	}
}
