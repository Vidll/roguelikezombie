using UnityEngine;

public class EnemyToxicBullet : Bullet
{
	[SerializeField] private ToxicPuddle _toxicPuddlePrefab;
	[SerializeField] private float _puddleDamage;

	public float PuddleDamage { set { _puddleDamage = value; } get => _puddleDamage; }

	private void OnEnable()
	{
		_moved = false;
		GlobalEvents.MapCreated += DestroyToxicBullet;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.TryGetComponent(out Wall wall))
		{
			var puddle = Instantiate(
				_toxicPuddlePrefab,
				new Vector3(transform.position.x,0f,transform.position.z),
				Quaternion.identity);
			DestroyToxicBullet();
			puddle.Damage = _puddleDamage;
			
		}
		else if (other.gameObject.TryGetComponent(out Player player))
		{
			_bulletModel.SetActive(false);
			GlobalEvents.MapCreated -= DestroyToxicBullet;
			DestroyAfterAnim();
			player.GetDamage(_damage);
		}
	}

	private void DestroyToxicBullet()
	{
		GlobalEvents.MapCreated -= DestroyToxicBullet;
		Destroy(gameObject);
	}
}
