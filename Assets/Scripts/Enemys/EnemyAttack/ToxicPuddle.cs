using UnityEngine;

public class ToxicPuddle : MonoBehaviour
{
	[SerializeField] private float _damage;
	[SerializeField] private ParticleSystem _idleParticleSystem;
	[SerializeField] private ParticleSystem _hitParticleSystem;

	[SerializeField] private float _lifeTime;
	[SerializeField] private float _attackTime;
	[SerializeField] private float _currentLifeTime;
	[SerializeField] private float _currentAttackTime;

	public float Damage { set { _damage = value; } get => _damage; }

	private void OnEnable()
	{
		GlobalEvents.MapCreated += DestroyPuddle;
	}

	private void OnTriggerStay(Collider other)
	{
		if(other.gameObject.TryGetComponent(out Player player))
		{
			if(_currentAttackTime > _attackTime)
			{
				player.GetDamage(_damage);
				_currentAttackTime = 0;
			}
		}
	}

	private void Update()
	{
		_currentLifeTime += Time.deltaTime;
		_currentAttackTime += Time.deltaTime;
		if (_currentLifeTime >= _lifeTime)
			DestroyPuddle();
	}

	private void DestroyPuddle()
	{
		GlobalEvents.MapCreated -= DestroyPuddle;
		Destroy(gameObject);
	}
}
