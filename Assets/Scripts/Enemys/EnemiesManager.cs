using UnityEngine;
using System.Collections.Generic;

public class EnemiesManager : MyComponentClass
{
	[SerializeField] private MapManager _mapManager;
	[SerializeField] private Enemy[] _prefabsZombie;
	[SerializeField] private Transform[] _spawnPoints;

	[SerializeField] private int[] _enemyCount = {6, 12}; 

	[SerializeField] private List<Enemy> _enemies = new List<Enemy>();
	[SerializeField] private bool _waveCreated = false;

	public Transform[] SpawnPoints { set { _spawnPoints = value; } }

	public override void StartGame()
	{
		base.StartGame();
		GlobalEvents.KillZombie += KillZombie;
		GlobalEvents.PassedWave += ChangeWave;
		GlobalEvents.MapCreated += SetValuesAndCreateZombies;
		_mapManager = GetComponentInParent<MapManager>();
	}

	public override void PlayGame()
	{
		base.PlayGame();
	}

	public override void LoseGame()
	{
		base.LoseGame();
	}

	public override void ReloadGame()
	{
		base.ReloadGame();
		DeleteAllZombies();
	}

	private void SetValuesAndCreateZombies()
	{
		_spawnPoints = _mapManager.CurrentMap.EnemiesSpawnPoints;
		if (!_waveCreated)
			CreateZombies();
	}

	private void CreateZombies()
	{
		_waveCreated = true;
		for (int i = 0; i < Random.Range(_enemyCount[0], _enemyCount[1]); i++)
		{
			var enemy = _prefabsZombie[Random.Range(0, _prefabsZombie.Length)];
			var point = _spawnPoints[Random.Range(0, _spawnPoints.Length)];

			var createdEnemy = Instantiate(enemy, point.position, Quaternion.identity);
			_enemies.Add(createdEnemy);
		}
	}

	private void CreateZombieWawe()
	{
		GlobalInterfaceEvents.IncreacePlayer -= CreateZombieWawe;
		if(!_waveCreated)
			CreateZombies();
	}

	private void KillZombie(Enemy enemy)
	{
		_enemies.Remove(enemy);
		if(_enemies.Count <= 0)
		{
			_waveCreated = false;
			GlobalParameters.Level = GlobalParameters.Level + 1;
			print("Level: " + GlobalParameters.Level);
			GlobalEvents.PassedWave?.Invoke();
		}
	}

	private void DeleteAllZombies()
	{
		foreach(Enemy enemy in _enemies)
			Destroy(enemy.gameObject);

		_waveCreated = false;
		_enemies.Clear();
	}

	private void ChangeWave() => GlobalInterfaceEvents.IncreacePlayer += CreateZombieWawe;
}
