using UnityEngine;
using UnityEngine.AI;

public abstract class Enemy : Character
{
	[Header("Enemy")]
	[SerializeField] protected Player _player;
	[SerializeField] protected Transform _playerTransform;

	[SerializeField] protected float _movedSpeed;
	[SerializeField] protected float _attackDistance;
	[SerializeField] protected float _attackTimeDelay;
	[SerializeField] protected float _rotateSpeed;
	[SerializeField] protected float _maxSpeedValue;

	[SerializeField] protected TypeAttack _currentTypeAttack;

	protected NavMeshAgent _navMeshAgent;
	protected bool _moved = true;
	protected float _currentTime;

	public override void StartCharacter()
	{
		base.StartCharacter();
		_player = FindObjectOfType<Player>();
		_navMeshAgent = GetComponent<NavMeshAgent>();

		SetLevelParameters();

		_playerTransform = _player.transform;
		_navMeshAgent.speed = _movedSpeed;
		_navMeshAgent.stoppingDistance = _attackDistance;
	}

	protected virtual void SetLevelParameters()
	{
		Damage = Damage + (GlobalParameters.LevelDivisionCoefficient * GlobalParameters.Level);
		SetAttackParameters();

		if (_movedSpeed >= _maxSpeedValue)
			return;
		_movedSpeed = _movedSpeed + (GlobalParameters.LevelDivisionCoefficient * GlobalParameters.Level);
	}

	protected virtual void SetAttackParameters()
	{

	}

	public override void Die()
	{
		base.Die();
		GlobalEvents.KillZombie?.Invoke(this);
		Destroy(gameObject);
	}

	private void Update()
	{
		_currentTime += Time.deltaTime;

		CheckDistance();
		if (_moved)
			Moved();
	}

	protected virtual void CheckDistance()
	{
		if (!_playerTransform)
			return;

		if(_attackDistance >= (_playerTransform.position - MyTransform.position).magnitude)
		{
			_moved = false;
			Rotate();
			if (_currentTypeAttack == TypeAttack.Range)
			{
				if (CheckTimeAttack())
					Attack();
			}
		}
		else
			_moved = true;
	}

	private void Rotate()
	{
		_myTransform.rotation = Quaternion.LerpUnclamped(
			_myTransform.transform.rotation,
			Quaternion.LookRotation(_playerTransform.position - _myTransform.position, Vector3.up),
			_rotateSpeed * Time.deltaTime);
	}

	private void Moved()
	{
		if(_playerTransform)
			_navMeshAgent.destination = _playerTransform.position;
	}

	protected virtual void Attack()
	{

	}

	public bool CheckTimeAttack()
	{
		if(_currentTime > _attackTimeDelay)
		{
			_currentTime = 0;
			return true;
		}
		else
			return false;
	}

	protected enum TypeAttack
	{
		NONE = 0,
		Closed,
		Range,
		Bomb,
		Boss,
	}
}