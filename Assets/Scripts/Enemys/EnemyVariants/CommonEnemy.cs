using UnityEngine;

public class CommonEnemy : Enemy
{
	[SerializeField] private CloseAttack _closeAttack;

	protected override void SetAttackParameters()
	{
		base.SetAttackParameters();
		_closeAttack.Damage = Damage;
	}
}
