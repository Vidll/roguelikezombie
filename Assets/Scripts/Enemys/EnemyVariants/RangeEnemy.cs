using UnityEngine;

public class RangeEnemy : Enemy
{
	[Header("Range enemy setting")]
	[SerializeField] private Transform _bulletSpawn;
	[SerializeField] private EnemyToxicBullet _bullet;
	[SerializeField] private float _puddleDamage;

	[SerializeField] private float _angleInDegrees = 45;

	private float gravityValue = Physics.gravity.y;

	protected override void Attack()
	{
		base.Attack();

		Vector3 fromTo = _playerTransform.position - _myTransform.position;
		Vector3 fromToXZ = new Vector3(fromTo.x, 0f, fromTo.z);

		float x = fromToXZ.magnitude;
		float y = fromToXZ.y;

		float angleInRadians = _angleInDegrees * Mathf.PI / 180;

		float v2 = (gravityValue * x * x) / (2 * (y - Mathf.Tan(angleInRadians) * x) * Mathf.Pow(Mathf.Cos(angleInRadians), 2));
		float v = Mathf.Sqrt(Mathf.Abs(v2));

		var bullet = Instantiate(_bullet, _bulletSpawn.position, Quaternion.identity);
		bullet.GetComponent<Rigidbody>().velocity = _bulletSpawn.forward * v;
		bullet.Damage = Damage;
		bullet.PuddleDamage = _puddleDamage;
	}

	protected override void SetAttackParameters()
	{
		base.SetAttackParameters();
		_puddleDamage = _puddleDamage + (GlobalParameters.LevelDivisionCoefficient * GlobalParameters.Level);
	}
}
