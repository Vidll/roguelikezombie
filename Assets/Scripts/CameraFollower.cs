using UnityEngine;

public class CameraFollower : MyComponentClass
{
	[SerializeField] private Transform _followTraget;
	[SerializeField] private Vector3 _offsetPosition;
	[SerializeField] private float _movedSpeed = 10;

	private Transform _myTrasnform;
	private bool _follow = false;

	public Transform FollowObject 
	{
		set 
		{
			_followTraget = value;
			_follow = true;
		}
	}

	public override void StartGame()
	{
		base.StartGame();
		_myTrasnform = GetComponent<Transform>();
	}

	public override void PlayGame()
	{
		base.PlayGame();
	}

	public override void LoseGame()
	{
		base.LoseGame();
		_follow = false;
	}

	public override void ReloadGame()
	{
		base.ReloadGame();
		_follow = true;
	}

	private void Update()
	{
		if (!_follow || !_followTraget)
			return;

		transform.position = Vector3.Lerp(
			_myTrasnform.position,
			_followTraget.position + _offsetPosition,
			_movedSpeed * Time.deltaTime);
	}
}
