using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class CameraHealthEffects : MonoBehaviour
{
    [SerializeField] private PostProcessProfile _cameraHealthProfile;
	[SerializeField] private Vignette _vignette;

	[SerializeField] private float _startPulseTime = 1f;
	[SerializeField] private float _endPulseTime = 1f;
	[SerializeField] private float _pulseValue = 0.5f;

	[SerializeField] private bool _pulseActive = false;
	[SerializeField] private bool _lowHealthActive = false;

	private void Start()
	{
		_vignette = _cameraHealthProfile.GetSetting<Vignette>();
		SetZero();

		GlobalInterfaceEvents.GetDamage += StartPulseGetDamage;
		GlobalInterfaceEvents.GetHealth += StartPulseGetHealth;
		GlobalInterfaceEvents.ChangeHealth += CheckPlayerHealth;
		//�������� ������ ��������� � ���������� �������� �������� �� ��
	}

	IEnumerator Pulse(float endValue, float minValue, bool skip)
	{
		bool first = true;
		bool second = true;
		float time = 0;

		while (first)
		{
			_vignette.intensity.value = Mathf.Lerp(_vignette.intensity.value, endValue, time);
			time += Time.deltaTime / _startPulseTime;
			yield return new WaitForSeconds(.05f);
			if (_vignette.intensity.value > (endValue - 0.05f))
			{
				first = false;
				time = 0;
			}
		}

		while (second)
		{
			_vignette.intensity.value = Mathf.Lerp(_vignette.intensity.value, minValue, time);
			time += Time.deltaTime / _endPulseTime;
			yield return new WaitForSeconds(.05f);
			if (_vignette.intensity.value < (minValue + 0.05f))
			{
				second = false;
				time = 0;
			}
		}
		if (!skip)
			SetZero();
		else if (skip)
			RepeatLowHealthPulse();
	}

	private void CheckPlayerHealth(float health, float maxHealth)
	{

	}

	[ContextMenu("StartPulseGetDamage")]
	private void StartPulseGetDamage()
	{
		if (_pulseActive)
			DisablePulse();
		else if (_lowHealthActive)
			return;

		_vignette.color.value = Color.red;
		_pulseActive = true;
		StartCoroutine(Pulse(_pulseValue, 0, false));
	}

	[ContextMenu("StartPulseGetHealth")]
	private void StartPulseGetHealth()
	{
		if (_pulseActive)
			DisablePulse();
		else if (_lowHealthActive)
			StopLowHealthPulse();

		_vignette.color.value = Color.green;
		_pulseActive = true;
		StartCoroutine(Pulse(_pulseValue, 0, false));
	}

	[ContextMenu("StartLowHealthPulse")]
	private void StartLowHealthPulse()
	{
		if (_pulseActive)
			DisablePulse();
		else if (_lowHealthActive)
			StopLowHealthPulse();

		_vignette.color.value = Color.red;
		_lowHealthActive = true;
		StartCoroutine(Pulse(_pulseValue, 0.3f, true));
	}

	private void RepeatLowHealthPulse()
	{
		if (_lowHealthActive)
			StartCoroutine(Pulse(_pulseValue, 0.3f, true));
		else
			SetZero();
	}

	[ContextMenu("StopLowHealthPulse")]
	private void StopLowHealthPulse()
	{
		StopCoroutine(Pulse(_pulseValue, 0.3f, true));
		_lowHealthActive = false;
	}

	private void DisablePulse()
	{
		StopCoroutine(Pulse(_pulseValue, 0, false));
		SetZero();
	}

	private void SetZero()
	{
		_vignette.intensity.value = 0;
		_pulseActive = false;
		_lowHealthActive = false;
	}

	private void OnDestroy()
	{
		GlobalInterfaceEvents.GetDamage -= StartPulseGetDamage;
		GlobalInterfaceEvents.GetHealth -= StartPulseGetHealth;
	}
}