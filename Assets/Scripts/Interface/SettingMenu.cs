using UnityEngine;
using UnityEngine.UI;

public class SettingMenu : Menu
{
	[SerializeField] private Button _openButton;
	[SerializeField] private Button _closeButton;

	public override void StartGame()
	{
		base.StartGame();

		if (_openButton)
			_openButton.onClick.AddListener(EnableWindow);
		if (_closeButton)
			_closeButton.onClick.AddListener(DisableWindow);
	}

	protected override void EnableWindow()
	{
		base.EnableWindow();
	}

	protected override void DisableWindow()
	{
		base.DisableWindow();
	}
}
