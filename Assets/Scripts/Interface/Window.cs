using UnityEngine;

public class Window : MonoBehaviour
{
    private bool _state;

    public bool WindowState { get => _state; set { _state = value; } }
}
