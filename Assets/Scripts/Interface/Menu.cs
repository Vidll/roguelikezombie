using UnityEngine;

public class Menu : MyComponentClass
{
	[SerializeField] protected Window _window;

	protected virtual void EnableWindow()
	{
		if(_window)
			_window.gameObject.SetActive(true);
	}

	protected virtual void DisableWindow()
	{
		if(_window)
			_window.gameObject.SetActive(false);
	}
}
