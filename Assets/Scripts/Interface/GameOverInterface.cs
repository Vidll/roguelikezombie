using UnityEngine;
using UnityEngine.UI;

public class GameOverInterface : Menu
{
	[SerializeField] private Button _restartButton;

	public override void StartGame()
	{
		base.StartGame();
		GlobalEvents.LoseGame += EnableWindow;

		if (_restartButton)
			_restartButton.onClick.AddListener(ReloadOnClick);
	}

	public void ReloadOnClick()
	{
		DisableWindow();
		GlobalEvents.ReloadGame?.Invoke();
	}
}
