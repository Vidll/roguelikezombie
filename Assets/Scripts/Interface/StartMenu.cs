using UnityEngine;
using UnityEngine.UI;

public class StartMenu : Menu
{
	[SerializeField] private Button _playGameButton;
	public override void StartGame()
	{
		base.StartGame();
		EnableWindow();
		if (_playGameButton)
			_playGameButton.onClick.AddListener(PlayGameOnClick);
	}

	private void PlayGameOnClick()
	{
		GlobalEvents.PlayGame?.Invoke();
		DisableWindow();
	}
}
