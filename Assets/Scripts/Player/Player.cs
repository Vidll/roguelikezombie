using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Player : Character
{
	[SerializeField] private Gun _currentGun;
	[SerializeField] private List<Enemy> _enemiesOnRadius;
	[SerializeField] private Enemy _currentTarget;
	[SerializeField] private Transform _gunUsagePosition;

	[SerializeField] private float _maxHealth;
	[SerializeField] private int _ammoIncreased;

	private PlayerMoverManager _mover;

	public float MaxHealth { set { _maxHealth += value; } }
	public float SpeedMove { set { _mover.MovedSpeed += value; } }
	public Gun CurrentGun { get => _currentGun; }

	public int Ammo 
	{
		set
		{
			_ammoIncreased = value;
			if(_currentGun)
				_currentGun.SetAmmo = _ammoIncreased; 
		}
		get => _ammoIncreased;
	}

	public override void StartCharacter()
	{
		base.StartCharacter();
		_mover = GetComponent<PlayerMoverManager>();
		PlayerIncrease.Init(this);
		Camera.main.GetComponent<CameraFollower>().FollowObject = _myTransform;
		GlobalEvents.KillZombie += DeleteZombie;
	}

	private void Update()
	{
		if (_enemiesOnRadius.Count > 0)
		{
			_currentTarget = _enemiesOnRadius.OrderByDescending(e => (e.MyTransform.position - MyTransform.position).magnitude).Last();
			_mover.RotateOnTarget(Quaternion.LookRotation(_currentTarget.MyTransform.position - MyTransform.position, Vector3.up));
		}
		else
		{
			_mover.IsRotateOnTarget = false;
			_currentTarget = null;
		}
	}

	public override void Die()
	{
		base.Die();
		GlobalEvents.LoseGame?.Invoke();
		GlobalParameters.ReloadLevelValue();
		Destroy(gameObject);
		print("Player Die");
	}

	public void SetGun(Gun gun)
	{
		if (_currentGun)
			DropGun();
		var createdGun = Instantiate(gun, _gunUsagePosition.position, _gunUsagePosition.rotation);
		createdGun.transform.parent = MyTransform;
		_currentGun = createdGun;
		_currentGun.SetAmmo = _ammoIncreased;
		createdGun.Init(this);
	}

	public void DropGun()
	{
		Destroy(_currentGun.gameObject);
	}

	public void OnClickShoot()
	{
		if(_currentGun && _currentTarget)
			_currentGun.Shoot(Damage);
	}

	public void IncreaseHealth(float value)
	{
		if (value > _maxHealth)
			Health = _maxHealth;
		else
			Health = value;
		GlobalInterfaceEvents.GetHealth?.Invoke();
		GlobalInterfaceEvents.ChangeHealth(Health, _maxHealth);
	}

	public override void GetDamage(float damage)
	{
		base.GetDamage(damage);
		//print("Get damage: " + damage);
		GlobalInterfaceEvents.GetDamage?.Invoke();
		GlobalInterfaceEvents.ChangeHealth(Health, _maxHealth);
	}

	public void SetZombie(Enemy enemy) => _enemiesOnRadius.Add(enemy);
	public void DeleteZombie(Enemy enemy) => _enemiesOnRadius.Remove(enemy);
}
