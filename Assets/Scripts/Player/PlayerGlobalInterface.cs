using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerGlobalInterface : Menu
{
	[SerializeField] private Window _increaseWindow;
	[SerializeField] private Window _nextMapTextWindow;
	[SerializeField] private TextMeshProUGUI _ammoText;
	[SerializeField] private Button _reloadGunButton;

	public override void StartGame()
	{
		base.StartGame();
		GlobalInterfaceEvents.ChangeAmmo += SetAmmoText;
		GlobalEvents.PassedWave += OpenIncreaseWindow;
		GlobalEvents.PlayGame += EnableWindow;

		if (_reloadGunButton)
			_reloadGunButton.onClick.AddListener(ReloadGunOnClick);
	}

	private void SetAmmoText(int value, int ammo)
	{
		if (_ammoText)
			_ammoText.text = value.ToString() + "/" + ammo.ToString();
	}

	private void OpenIncreaseWindow()
	{
		_increaseWindow.gameObject.SetActive(true);
	}

	private void ReloadGunOnClick()
	{
		GlobalEvents.ReloadGun?.Invoke();
	}

	public void IncreaseOnClick(int value)
	{
		IncreaseType type = (IncreaseType)value;
		switch (type)
		{
			case IncreaseType.NONE:
				GlobalInterfaceEvents.IncreacePlayer?.Invoke();
				_increaseWindow.gameObject.SetActive(false);
				break;
			case IncreaseType.Health:
				if (PlayerIncrease.IncreaseHealth())
					goto case IncreaseType.NONE;
				else
					break;
			case IncreaseType.Speed:
				if (PlayerIncrease.IncreaseSpeed())
					goto case IncreaseType.NONE;
				else
					break;
			case IncreaseType.Damage:
				if (PlayerIncrease.IncreaseDamage())
					goto case IncreaseType.NONE;
				else
					break;
			case IncreaseType.Ammo:
				if (PlayerIncrease.IncreaseAmmo())
					goto case IncreaseType.NONE;
				else
					break;
		}
	}

	public enum IncreaseType
	{
		NONE,
		Health,
		Speed,
		Damage,
		Ammo,
	}
}
