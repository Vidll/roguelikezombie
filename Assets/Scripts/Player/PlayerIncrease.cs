public struct ValuesIncreasePlayer 
{
	public int _currentLevelHealth;
	public int _currentLevelSpeed;
	public int _currentLevelDamage;
	public int _currentLevelAmmo;

	public int _maxLevelSpeed;
}

public static class PlayerIncrease
{
	private static Player _player;

	private static int _healthValueIncrease = 10;
	private static int _damageValueIncrease = 2;
	private static int _ammoValueIncrease = 1;
	private static float _speedValueIncrease = 0.5f;

	private static ValuesIncreasePlayer _myValuesIncreasePlayer = new ValuesIncreasePlayer();

	public static void Init(Player player)
	{
		_player = player;
		SetValuesOnPlayer();
	}

	public static bool IncreaseHealth()
	{
		_myValuesIncreasePlayer._currentLevelHealth++;
		_player.MaxHealth = _healthValueIncrease;
		return true;
	}

	public static bool IncreaseSpeed()
	{
		if (_myValuesIncreasePlayer._currentLevelSpeed < _myValuesIncreasePlayer._maxLevelSpeed)
		{
			_myValuesIncreasePlayer._currentLevelSpeed++;
			_player.SpeedMove = _speedValueIncrease;
			return true;
		}
		else
			return false;
	}

	public static bool IncreaseDamage()
	{
		_myValuesIncreasePlayer._currentLevelDamage++;
		_player.Damage = _damageValueIncrease; 
		return true;
	}

	public static bool IncreaseAmmo()
	{
		_myValuesIncreasePlayer._currentLevelAmmo++;
		_player.Ammo = _ammoValueIncrease * _myValuesIncreasePlayer._currentLevelAmmo;
		return true;
	}

	public static void SetDefaultIncreaseValues()
	{
		_myValuesIncreasePlayer._currentLevelHealth = 0;
		_myValuesIncreasePlayer._currentLevelSpeed = 0;
		_myValuesIncreasePlayer._currentLevelDamage = 0;
		_myValuesIncreasePlayer._currentLevelAmmo = 0;

		_myValuesIncreasePlayer._maxLevelSpeed = 5;
	}

	public static void SetValuesOnPlayer()
	{
		_player.MaxHealth = _healthValueIncrease * _myValuesIncreasePlayer._currentLevelHealth;
		_player.SpeedMove = _speedValueIncrease * _myValuesIncreasePlayer._currentLevelSpeed;
		_player.Damage = _damageValueIncrease * _myValuesIncreasePlayer._currentLevelDamage;
		_player.Ammo = _ammoValueIncrease * _myValuesIncreasePlayer._currentLevelAmmo;
	}
}
