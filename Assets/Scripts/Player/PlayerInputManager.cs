using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerInputManager : MonoBehaviour
{
    [SerializeField] private bool _mobileInput;

	[SerializeField] private Button _shootButton;
	[SerializeField] private Gamepad _gamePad;

	private PlayerMoverManager _mover;
	private Player _player;
	private Vector3 _movedVector = Vector3.zero;

	private void OnEnable()
	{
		_mover = GetComponent<PlayerMoverManager>();
		_player = GetComponent<Player>();

		if (_mobileInput)
		{
			_gamePad.Init();
			_shootButton.gameObject.SetActive(true);
			_shootButton.onClick.AddListener(ClickShoot);//�������� ��� ������ ����� ������
		}
	}

	private void Update()
	{
		if (_mobileInput)
			return;

		if (Input.GetMouseButton(0))
			ClickShoot();

		if (Input.GetKey(KeyCode.W))
		{
			_movedVector = new Vector3(_movedVector.x, _movedVector.y, 1);
		}
		else if (Input.GetKey(KeyCode.S))
			_movedVector = new Vector3(_movedVector.x, _movedVector.y, -1);
		else
			_movedVector = new Vector3(_movedVector.x, _movedVector.y, 0);

		if (Input.GetKey(KeyCode.A))
			_movedVector = new Vector3(-1, _movedVector.y, _movedVector.z);
		else if (Input.GetKey(KeyCode.D))
			_movedVector = new Vector3(1, _movedVector.y, _movedVector.z);
		else
			_movedVector = new Vector3(0, _movedVector.y, _movedVector.z);

		_mover.Moved(_movedVector);
		if (_movedVector != Vector3.zero)
			_mover.Rotate(_movedVector);
	}

	private void ClickShoot()
	{
		_player.OnClickShoot();
	}
}
