using UnityEngine;

public class PlayerMoverManager : MonoBehaviour
{
	[SerializeField] private float _movedSpeed;
	[SerializeField] private float _rotateSpeed;

	private bool _isRotateOnTarget = false;
	private Transform _myTransform;
	private CharacterController _controller;

	public bool IsRotateOnTarget { set { _isRotateOnTarget = value; } }
	public float MovedSpeed { set { _movedSpeed = value; } get => _movedSpeed; }


	private void OnEnable()
	{
		_myTransform = GetComponent<Transform>();
		_controller = GetComponent<CharacterController>();
	}

	public void StartMoved()
	{
		
	}

	public void EndMoved()
	{
		
	}

	public void Moved(Vector3 vector)
	{
		_controller.Move(vector * _movedSpeed * Time.deltaTime);
	}

	public void Rotate(Vector3 vector)
	{
		if (_isRotateOnTarget)
			return;

		_myTransform.rotation = Quaternion.LerpUnclamped(
			_myTransform.transform.rotation,
			Quaternion.LookRotation(vector),
			_rotateSpeed * Time.deltaTime);
	}

	public void RotateOnTarget(Quaternion vector)
	{
		_isRotateOnTarget = true;
		_myTransform.rotation = Quaternion.LerpUnclamped(
			_myTransform.transform.rotation,
			vector,
			_rotateSpeed * Time.deltaTime);
	}
}