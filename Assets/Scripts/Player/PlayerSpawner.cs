using UnityEngine;

public class PlayerSpawner : MyComponentClass
{
	[SerializeField] private MapManager _mapManager;
	[SerializeField] private Player _playerPrefab;
	[SerializeField] private Transform _spawnPoint;

	private Player _currentPlayer;

	public Transform SpawnPoint { get => _spawnPoint; set { _spawnPoint = value; } }

	public override void StartGame()
	{
		base.StartGame();
		GlobalEvents.MapCreated += SetValuesAndCreatePlayer;
		_mapManager = GetComponentInParent<MapManager>();
		GlobalEvents.StartChangeMap += DeletePlayer;
	}

	public override void PlayGame()
	{
		base.PlayGame();
		PlayerIncrease.SetDefaultIncreaseValues();
	}

	public override void ReloadGame()
	{
		base.ReloadGame();
		if(_currentPlayer)
			DeletePlayer();
	}

	public override void LoseGame()
	{
		base.LoseGame();
	}

	private void SetValuesAndCreatePlayer()
	{
		_spawnPoint = _mapManager.CurrentMap.PlayerSpawnPoint;
		CreatePlayer();
	}

	private void CreatePlayer()
	{
		_currentPlayer = Instantiate(_playerPrefab, _spawnPoint.position, Quaternion.identity);
		_currentPlayer.gameObject.SetActive(true);
		_currentPlayer.transform.parent = transform;
	}

	private void DeletePlayer() => Destroy(_currentPlayer.gameObject);
}
