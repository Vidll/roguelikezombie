using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerLocalInterface : MonoBehaviour
{
    [SerializeField] private GameObject _reloading;
	[SerializeField] private Image _healthImage;

	private Canvas _playerCanvas;

	private void OnEnable()
	{
		GlobalInterfaceEvents.StartReload += EnableReloading;
		GlobalInterfaceEvents.ChangeHealth += SetHealth;

		if(!_reloading || !_healthImage)
			GetMyComponents();

		_reloading.SetActive(false);
	}

	private void OnDestroy()
	{
		GlobalInterfaceEvents.StartReload -= EnableReloading;
		GlobalInterfaceEvents.ChangeHealth -= SetHealth;
		GlobalInterfaceEvents.FinishReload -= DisableReloading;
	}

	private void GetMyComponents()
	{
		_playerCanvas = GetComponentInChildren<Canvas>();
		_reloading = _playerCanvas.GetComponentInChildren<TextMeshProUGUI>().gameObject;
		_healthImage = _playerCanvas.GetComponentInChildren<Image>();
	}

	private void EnableReloading()
	{
		GlobalInterfaceEvents.StartReload -= EnableReloading;
		GlobalInterfaceEvents.FinishReload += DisableReloading;
		_reloading.gameObject.SetActive(true);
	}

	private void DisableReloading()
	{
		GlobalInterfaceEvents.StartReload += EnableReloading;
		GlobalInterfaceEvents.FinishReload -= DisableReloading;
		_reloading.gameObject.SetActive(false);
	}

	private void SetHealth(float currentValue, float maxHealth)
	{
		var precent = (currentValue * 100) / maxHealth;
		_healthImage.fillAmount = precent / 100;
	}
}
