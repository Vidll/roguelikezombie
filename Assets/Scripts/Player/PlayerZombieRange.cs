using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerZombieRange : MonoBehaviour
{
    [SerializeField] private Player _player;

	private void OnEnable()
	{
		_player = transform.parent.GetComponent<Player>();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.TryGetComponent(out Enemy enemy))
			_player.SetZombie(enemy);
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.TryGetComponent(out Enemy enemy))
			_player.DeleteZombie(enemy);
	}
}
