public static class GlobalInterfaceEvents 
{
    public delegate void PlayerInterfaceEvent();
    public delegate void PlayerInterfaceEventDoubleInt(int firstValue, int secondValue);
    public delegate void PlayerInterfaceEventDoubleFloat(float FirstValue, float secondValue);


    public static PlayerInterfaceEvent StartReload;
    public static PlayerInterfaceEvent FinishReload;

    public static PlayerInterfaceEvent IncreacePlayer;

    public static PlayerInterfaceEventDoubleInt ChangeAmmo;

    public static PlayerInterfaceEventDoubleFloat ChangeHealth;
    public static PlayerInterfaceEvent GetDamage;
    public static PlayerInterfaceEvent GetHealth;
}
