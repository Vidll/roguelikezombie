using UnityEngine;

public abstract class MyComponentClass : MonoBehaviour
{
	protected bool _startPlay = false;

	public virtual void StartGame()
	{

	}

    public virtual void PlayGame()
	{
		_startPlay = true;
	}

	public virtual void LoseGame()
	{

	}

	public virtual void ReloadGame()
	{

	}
}
