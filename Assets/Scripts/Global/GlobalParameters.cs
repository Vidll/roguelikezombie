public static class GlobalParameters
{
	public static int Level = 1;

	public static float LevelDivisionCoefficient = 0.3f;

    public static void ReloadLevelValue()
	{
		Level = 1;
	}
}