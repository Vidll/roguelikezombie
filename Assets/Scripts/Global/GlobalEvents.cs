public static class GlobalEvents
{
    public delegate void GameEvent();
    public delegate void GameEventEnemy(Enemy enemy);

    public static GameEvent PlayGame;
    public static GameEvent LoseGame;
    public static GameEvent ReloadGame;

    public static GameEvent PassedRoom;
    public static GameEvent PassedWave;

	public static GameEventEnemy KillZombie;

    public static GameEvent ReloadGun;

    public static GameEvent StartChangeMap;
    public static GameEvent MapCreated;
}
