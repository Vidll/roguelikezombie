using UnityEngine;

public class GameState : MonoBehaviour
{
	[SerializeField] private MyComponentClass[] _myClasses;

	private void Start()
	{
		_myClasses = FindObjectsOfType<MyComponentClass>();
		GlobalEvents.PlayGame += InitPlayGame;
		GlobalEvents.ReloadGame += ReloadGame;
		InitStartGame();
	}

	private void InitStartGame()
	{
		foreach (MyComponentClass myClass in _myClasses)
			myClass.StartGame();
	}

	[ContextMenu("PlayGame")]
	private void InitPlayGame()
	{
		foreach (MyComponentClass myClass in _myClasses)
			myClass.PlayGame();
	}

	[ContextMenu("LoseGame")]
	private void InitLoseGame()
	{
		foreach (MyComponentClass myClass in _myClasses)
			myClass.LoseGame();
	}

	[ContextMenu("ReloadGame")]
	private void ReloadGame()
	{
		foreach (MyComponentClass myClass in _myClasses)
			myClass.ReloadGame();

		GlobalEvents.PlayGame?.Invoke();
	}
}
