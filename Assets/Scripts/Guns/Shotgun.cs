using UnityEngine;

public class Shotgun : Gun
{
	[SerializeField] private float _bulletAngle = 35f;
	[SerializeField] private float _bulletsCount = 20;

	public override void Shoot(float valueDamageIncrease)
	{
		if (CheckTimer())
		{
			base.Shoot(valueDamageIncrease);
			for(int i = 0; i < _bulletsCount; i++)
			{
				var newAngle = Quaternion.Euler(0, Random.Range(-_bulletAngle, _bulletAngle), 0f) * _myTransform.parent.transform.rotation;
				var newBullet = Instantiate(_bulletPrefab,_spawnBulletPoint.position, newAngle);
				newBullet.Damage = _bulletDamage + valueDamageIncrease;
				newBullet.MoveSpeed = _bulletSpeed;
			}
		}
	}
}
