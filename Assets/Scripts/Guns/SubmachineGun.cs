using UnityEngine;

public class SubmachineGun : Gun
{
	public override void Shoot(float valueDamageIncrease)
	{
		if (CheckTimer())
		{
			base.Shoot(valueDamageIncrease);
			var bullet = Instantiate(_bulletPrefab, _spawnBulletPoint.position, _myTransform.parent.transform.rotation);
			bullet.Damage = _bulletDamage + valueDamageIncrease;
			bullet.MoveSpeed = _bulletSpeed;
		}
	}
}