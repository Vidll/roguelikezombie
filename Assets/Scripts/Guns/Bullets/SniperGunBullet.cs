using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperGunBullet : Bullet
{
	[SerializeField] private int _countCollision = 3;
	[SerializeField] private List<ParticleSystem> _createdParticles = new List<ParticleSystem>();

	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.TryGetComponent(out Enemy enemy))
		{
			_countCollision--;
			enemy.GetDamage(_damage);
			SetPaticleToEnemy(enemy);
			if (_countCollision <= 0)
			{
				_moved = false;
				_bulletModel.SetActive(false);
				StartCoroutine(DestroyAfterAnim());
			}
		}
		else if (other.gameObject.TryGetComponent(out Wall wall))
		{
			_moved = false;
			Destroy(gameObject);
		}
	}

	private void SetPaticleToEnemy(Enemy enemy)
	{
		var newPaticle = Instantiate(_bloodParticle, transform.position, transform.rotation);
		_createdParticles.Add(newPaticle);
		newPaticle.transform.parent = enemy.transform;
		newPaticle.Play();
	}

	private void OnDestroy()
	{
		foreach(ParticleSystem particle in _createdParticles)
		{
			if(particle)
				Destroy(particle.gameObject);
		}
		_createdParticles.Clear();
	}
}
