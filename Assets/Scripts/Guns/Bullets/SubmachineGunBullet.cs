using UnityEngine;

public class SubmachineGunBullet : Bullet
{
	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.TryGetComponent(out Enemy enemy) && _moved)
		{
			_moved = false;
			_myTransform.parent = enemy.MyTransform;
			enemy.GetDamage(_damage);
			_bloodParticle.Play();
			_bulletModel.SetActive(false);
			StartCoroutine(DestroyAfterAnim());
		}
		else if (collision.gameObject.TryGetComponent(out Wall wall))
		{
			_moved = false;
			Destroy(gameObject);
		}
	}
}
