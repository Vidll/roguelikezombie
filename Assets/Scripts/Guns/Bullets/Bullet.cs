using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
	[SerializeField] protected float _moveSpeed = 20f;
	[SerializeField] protected float _timeToDestroy = 1f;
	[SerializeField] protected float _damage;

	[SerializeField] protected ParticleSystem _bloodParticle;
	[SerializeField] protected ParticleSystem _flyParticle;

	[SerializeField] protected GameObject _bulletModel;

	protected Transform _myTransform;
	protected bool _moved = true;

	public float Damage { set { _damage = value; } }
	public float MoveSpeed { set { _moveSpeed = value; } }
	public Transform MyTansform { get => _myTransform; }

	private void OnEnable()
	{
		_myTransform = GetComponent<Transform>();
	}

	private void Update()
	{
		if (_moved)
			_myTransform.Translate(Vector3.forward * _moveSpeed * Time.deltaTime);
	}

	protected virtual IEnumerator DestroyAfterAnim()
	{
		yield return new WaitForSeconds(_timeToDestroy);
		Destroy(gameObject);
	}
}
