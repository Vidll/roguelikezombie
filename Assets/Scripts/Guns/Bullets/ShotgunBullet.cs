using UnityEngine;

public class ShotgunBullet : Bullet
{
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.TryGetComponent(out Enemy enemy))
		{
			_moved = false;
			_bulletModel.SetActive(false);
			enemy.GetDamage(_damage);
			StartCoroutine(DestroyAfterAnim());
		} 
		else if (other.gameObject.TryGetComponent(out Wall wall))
		{
			_moved = false;
			Destroy(gameObject);
		}
	}
}
