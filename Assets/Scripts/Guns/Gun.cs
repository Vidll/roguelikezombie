using UnityEngine;
using System.Collections;

public abstract class Gun : MonoBehaviour
{
    [SerializeField] protected Transform _spawnBulletPoint;
    [SerializeField] protected Bullet _bulletPrefab;
    [SerializeField] protected ParticleSystem _shootParticle;

    [SerializeField] protected float _bulletSpeed;
    [SerializeField] protected float _bulletDamage;
    [SerializeField] protected float _shootTimeDelay;
    [SerializeField] protected float _reloadTimeDelay;

    [SerializeField] protected int _startAmmo;
    [SerializeField] protected int _allAmmo;
    [SerializeField] protected int _currentAmmo;

    protected Transform _myTransform;
    protected float _currentTime = 0f;
    protected bool _reloading = false;

    public int SetAmmo 
    { 
        set 
        {
            _allAmmo = _startAmmo + value;
            ChangeAmmoOnInterface();
        }
        get => _allAmmo;
    }

    public void Init(Player player)
	{
        _myTransform = GetComponent<Transform>();
        _currentAmmo = _startAmmo + player.Ammo;
        GlobalEvents.ReloadGun += ReloadAmmo;
        GlobalInterfaceEvents.ChangeAmmo?.Invoke(_currentAmmo, _allAmmo);
    }

	private void OnDestroy()
	{
        GlobalEvents.ReloadGun -= ReloadAmmo;
    }

    private void Update()
	{
        _currentTime += Time.deltaTime;
	}

    public virtual void Shoot(float valueDamageIncrease)
    {
        if (_shootParticle)
            _shootParticle.Play();
        _currentTime = 0;
        _currentAmmo--;
        GlobalInterfaceEvents.ChangeAmmo?.Invoke(_currentAmmo, _allAmmo);
        if (_currentAmmo <= 0)
            StartCoroutine(Reloading());
    }

    [ContextMenu("ReloadAmmo")]
    public virtual void ReloadAmmo()
	{
        StartCoroutine(Reloading());
	}

    public bool CheckTimer()
	{
        if (!_reloading)
            return _currentTime > _shootTimeDelay ? true : false;
        else
            return false;
    }

    public IEnumerator Reloading()
	{
        _reloading = true;
        GlobalInterfaceEvents.StartReload?.Invoke();
        yield return new WaitForSeconds(_reloadTimeDelay);
        _reloading = false;
        _currentAmmo = _allAmmo;
        ChangeAmmoOnInterface();
		GlobalInterfaceEvents.FinishReload?.Invoke();
	}

    private void ChangeAmmoOnInterface()
	{
        GlobalInterfaceEvents.ChangeAmmo?.Invoke(_currentAmmo, _allAmmo);
    }
}