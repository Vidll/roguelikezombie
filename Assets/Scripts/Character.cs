using UnityEngine;

public abstract class Character : MonoBehaviour
{
    [SerializeField] private float _health;
    [SerializeField] private float _damage;

    [SerializeField] private bool _dontDie = false;

    protected Transform _myTransform;

    protected bool _isDie = false;

    public float Damage { get => _damage; set { _damage = value; } }
    public Transform MyTransform { get => _myTransform; }
    public float Health { get => _health; set { _health = value; } }

	private void OnEnable()
	{
        _myTransform = GetComponent<Transform>();
        _isDie = false;
        StartCharacter();
	}

    public virtual void StartCharacter()
	{

	}

	public virtual void GetDamage(float damage)
	{
        if (_isDie)
            return;
        if (_dontDie)
            return;

        _health -= damage;
        if (_health <= 0)
            Die();
	}

    public virtual void Die()
	{
        _isDie = true;
	}
}
